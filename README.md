# _WORK IN PROGRESS_
# Découverte Linux From Scratch (LFS)

## 1. Introduction

### 1.1 Objectif

L'objectif ici est de suivre la documentation de LFS , afin d'approfondir mes connaissances en linux et en administration système.

### 1.2. Liens utiles

* [Lien vers la documentation officielle fr](http://fr.linuxfromscratch.org/)
* [Sommaire LFS 10.0](http://fr.linuxfromscratch.org/view/lfs-systemd-stable/)

Infos générales:
* Date de début de projet : 05/01/2021
* Version LFS: 10.0
* Syteme d'init : Systemd
* Machine virtuelle hôte:
  * Hyperviseur: VirtualBox
  * OS: ubuntu 18.04 LTS
  * CPU: 4
  * RAM: 8 Go

### 1.3. Prérequis

Afin de construire notre distribution Linux il nous faut respecter les prérequis suivants:
* Avoir un OS Linux hôte pour héberger notre distribution (ici il s'agit de la VM décrite précédemment)
* Avoir au minimum les [paquets suivants](http://fr.linuxfromscratch.org/view/lfs-systemd-stable/chapter02/hostreqs.html) installés. La vérification des différents paquets peut s'effectuer en lancant le script [version-check.sh](./scripts/bash/version-check.sh) issu de la documentation)

## [2. Partitionnement](./docs/Partitions_LFS.md)

### [2.1. Création et Ajout d'un media virtuel sous VBox](./docs/partitionnement/creation_ajout_media_VBox.md)

### [2.2 Montage des partitions avec LVM](./docs/partitionnement/lvm_partition.md)

## [3. Gestion des paquets](./docs/package.md)

## [4. Arborescence](./docs/tree.md)

## [5. Création d'un utilisateur dédié](./docs/lfs_user.md)
