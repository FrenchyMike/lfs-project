# Partitionnement

## Objectif

Afin de respecter les recommendations ([lien vers la doc officielle](http://fr.linuxfromscratch.org/view/lfs-systemd-stable/chapter02/creatingpartition.html)) de LFS, nous allons créer un disque partitionné dédié.

## [Création d'un media sous VirtualBox](./partitionnement/creation_ajout_media_VBox.md)

## [Partitionnement du disque via LVM](./partitionnement/lvm_partition.md)

### [Sommaire](../README.md)
