# Gestion des paquets basiques

Pour télécharger les paquets nécéssaires à l'installation de LFS lancer le [script suivant](../scripts/bash/dl-package.sh)
<br/>
Les paquets seront télécharger dans le dossier `$LFS/sources`

### [Sommaire](../README.md)