
En tant que root, créez une arborescence classique Linux:

```bash
mkdir -pv $LFS/{bin,etc,lib,sbin,usr,var}
case $(uname -m) in
  x86_64) mkdir -pv $LFS/lib64 ;;
esac

# tree -L 1 /LFS/
/LFS/
├── bin
├── boot
├── etc
├── home
├── lib
├── lib64
├── lost+found
├── sbin
├── sources
├── tools
├── usr
└── var
```

### [Sommaire](../README.md)