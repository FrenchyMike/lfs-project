# L'utilisateur `lfs`

## Creation du user et du groupe associé

```bash
groupadd lfs
useradd -s /bin/bash -g lfs -m -k /dev/null lfs
```

Détails de la commande :
* `-s /bin/bash` : Ceci fait de bash le shell par défaut de l'utilisateur lfs.
* `-g lfs`: Cette option ajoute l'utilisateur lfs au groupe lfs.
* `-m` : Ceci crée un répertoire personnel pour l'utilisateur lfs.
* `-k /dev/null`: Ce paramètre empêche toute copie possible de fichiers provenant du répertoire squelette (par défaut, /etc/skel) en modifiant son emplacement par le périphérique spécial null.
* `lfs` : Ceci est le nom réel pour l'utilisateur créé.

## Faire de `lfs` le propriétaire des dossiers de l'arborescence

```bash
chown -v lfs $LFS/{usr,lib,var,etc,bin,sbin,tools,sources}
case $(uname -m) in
  x86_64) chown -v lfs $LFS/lib64 ;;
esac
```

## Configuration de `.bash_profil` et `.bashrc`

### `.bash_profil`
```bash
cat > ~/.bash_profile << "EOF"
exec env -i HOME=$HOME TERM=$TERM PS1='\u:\w\$ ' /bin/bash
EOF
```

### `.bashrc`
```bash
cat > ~/.bashrc << "EOF"
set +h
umask 022
LFS=/mnt/lfs
LC_ALL=POSIX
LFS_TGT=$(uname -m)-lfs-linux-gnu
PATH=/usr/bin
if [ ! -L /bin ]; then PATH=/bin:$PATH; fi
PATH=$LFS/tools/bin:$PATH
export LFS LC_ALL LFS_TGT PATH
EOF
```

*Détails de la commande*

* `set +h`:
    La commande set +h désactive la fonction de hachage de bash. D'habitude, le hachage est une fonctionnalité utile—bash utilise une table de hachage pour se rappeler le chemin complet des fichiers exécutables pour éviter d'avoir à chercher dans PATH à chaque fois qu'il doit trouver le même exécutable. Néanmoins, les nouveaux outils devraient être utilisés dès leur installation. En désactivant la fonction de hachage, le shell cherchera en permanence dans PATH lorsqu'un programme doit être exécuté. Ainsi, le shell trouvera les nouveaux outils compilés dans $LFS/tools dès qu'ils sont disponibles et sans se rappeler la version précédente du même programme mais dans un autre emplacement.

* `umask 022`:
    Configurer le masque de création de fichier (umask) à 022 nous assure que les nouveaux fichiers et répertoires créés sont modifiables uniquement par leurs propriétaires mais lisibles et exécutables par tout le monde (en supposant que l'appel système open(2) utilise les modes par défaut, les nouveaux fichiers auront les droits 644 et les répertoires 755).

* `LFS=/mnt/lfs`:
    La variable LFS devrait être configurée avec le point de montage choisi.

* `LC_ALL=POSIX`:
    La variable LC_ALL contrôle les paramètres linguistiques de certains programmes, faisant que leurs messages suivent les conventions d'un pays spécifié. Définir LC_ALL à « POSIX » ou « C » (les deux étant équivalents) garantit que tout fonctionnera comme prévu dans l'environnement chroot.

* `LFS_TGT=(uname -m)-lfs-linux-gnu`:
    La variable LFS_TGT initialise une description de la machine personnalisée mais compatible lors de la construction de notre compilateur, de notre éditeur de liens croisés et lors de la compilation de notre chaîne d'outils temporaires. Vous trouverez plus d'informations dans les Notes techniques sur la chaîne d'outils.

* `PATH=/usr/bin`:
    De nombreuses distributions modernes ont fusionné /bin et /usr/bin. Lorsque c'est le cas, la variable PATH standard n'a besoin que d'indiquer /usr/bin pour l'environnement ue Chapitre 6. Lorsque ce n'est pas le cas, la ligne suivante ajoute /bin au chemin de recherche.

* `if [ ! -L /bin ]; then PATH=/bin:$PATH; fi`:
    Si /bin n'est pas un lien symbolique, il doit être ajouté à la variable PATH.

* `PATH=$LFS/tools/bin:$PATH`:
    En plaçant $LFS/tools/bin au début du PATH standard, le compilateur croisé installé au début du Chapitre 5 est repéré par le shell immédiatement après son installation. Ceci, combiné à la désactivation du hachage, limite le risque que le compilateur de l'hôte ne soit utilisé à la place du compilateur croisé.

* `export LFS LC_ALL LFS_TGT PATH`:
    Bien que les commandes précédentes aient configurées certaines variables, pour les rendre visibles à des sous-shell, nous les exportons.

### [Sommaire](../README.md)
