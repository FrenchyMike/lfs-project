# Création et ajout d'un media dans VBox

Pour prendre de la marge, nous allons partir sur un disque dur de 50Go. **Attention sur la capture d'écran il est écrit 30G**
Comme nous travaillons depuis une VM, nous allons créer un disque dur virtuel dédié:

1. Création du disque dans Virtual Box
<br/>
<kbd><img src="../images/VBox_media.png" width="600" heigh="600"/></kbd>
<br/><br/>
<kbd><img src="../images/VBox_media2.png" width="600" heigh="600"/></kbd>
<br/>
<br/>
2. Configurationd du disque (nommage et taille)
<br/>
<kbd><img src="../images/VBox_media3.png" width="600" heigh="600"/></kbd>
<br/>
<br/>
3. Ajout du disque à la machine
<br/>
<kbd><img src="../images/VBox_media4.png" width="1000" heigh="700"></kbd>
<br/>
<br/>

### [Sommaire](../../README.md)
