# Partitionnement du disque via LVM

Avant de se lancer dans le partitionnement à proprement parlé, veillez à avoir [ajouter un disque vierge virtuel sous VBox](./creation_ajout_media_VBox.md).
<br/>
Si vous n'utilisez pas de machine virtuelle veillez à avoir une partition vierge d'au moins 50Go

**Important ! la partition sera formatée pendant le processus, si le disque virtuel (ou partition dédiée) contient des données, elles seront effacées**

## Objectif 

Partitionner le disque `/dev/sdb` (créé et ajouté précédemment) en trois partitions logiques via [LVM](https://doc.ubuntu-fr.org/lvm), et les monter sur les FS suivants:
* /LFS : 20G
* /LFS/boot : 200M
* /LFS/home : 20G
<br/>*La dizaine de Gigas restants servira d'appoint au cas où*

**Prérequis**
<br/>Créer les repertoires:
  * `mkdir -p /LFS/boot`
  * `mkdir /LFS/home`

## Utilisation de LVM

Un chapitre a déjà été rédigé sur la création de volumes et leur montage sous LVM ([lien vers la doc](https://gitlab.com/FrenchyMike/mon-wiki/-/wikis/Montage-de-partitions-logiques)).
<br/>
Je ne rentrerai donc pas trop dans les détails

1. Création du volume physique: 
  * `$ sudo vgcreate LFS_vg /dev/sdb`
2. Partitionnement du volume physique en deux volumes logiques (boot et home):
  * `sudo lvcreate -L 20G -n root_lv LFS_vg`
  * `sudo lvcreate -L 200M -n boot_lv LFS_vg`
  * `sudo lvcreate -L 20G -n home_lv LFS_vg`
  
3. Formatage des partitions en ext4
  * `sudo mkfs.ext4 /dev/LFS_vg/root_lv`
  * `sudo mkfs.ext4 /dev/LFS_vg/boot_lv`
  * `sudo mkfs.ext4 /dev/LFS_vg/home_lv`
  
4. Montage des partitions:
  * `sudo mount /dev/LFS_vg/root_lv /LFS/`
  * `sudo mount /dev/LFS_vg/boot_lv /LFS/boot/`
  * `sudo mount /dev/LFS_vg/home_lv /LFS/home/`
5. Modification du fstab pour prise en compte du montage en cas de reboot:
<br/>*Ajouter les deux dernières lignes*
```bash
$ sudo vim /etc/fstab

# /etc/fstab: static file system information.
#
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
# / was on /dev/sda1 during installation
UUID=fa6aac3d-c08d-432c-9d5d-75d728683db7 /               ext4    errors=remount-ro 0       1
/swapfile                                 none            swap    sw              0       0

/dev/LFS_vg/root_lv /LFS   ext4    defaults    1   1
/dev/LFS_vg/boot_lv /LFS/boot   ext4    defaults    1   1
/dev/LFS_vg/home_lv /LFS/home   ext4    defaults    1   1
```

## Vérification

Pour vérifier les volumes logiques, lancer la commande suivante:

```bash
$ sudo lvdisplay
  --- Logical volume ---
  LV Path                /dev/LFS_vg/boot_lv
  LV Name                boot_lv
  VG Name                LFS_vg
  LV UUID                WKg1rt-qOAp-i6MM-mBcZ-OGWM-SdL2-uUyzkT
  LV Write Access        read/write
  LV Creation host, time bionic, 2021-01-06 09:36:02 +0100
  LV Status              available
  # open                 1
  LV Size                200,00 MiB
  Current LE             50
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:0
   
  --- Logical volume ---
  LV Path                /dev/LFS_vg/home_lv
  LV Name                home_lv
  VG Name                LFS_vg
  LV UUID                HT8VkK-DNQE-kXQY-Xrby-LMoA-GeU6-mxSU28
  LV Write Access        read/write
  LV Creation host, time bionic, 2021-01-06 09:37:30 +0100
  LV Status              available
  # open                 1
  LV Size                20,00 GiB
  Current LE             5120
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:1
   
  --- Logical volume ---
  LV Path                /dev/LFS_vg/root_lv
  LV Name                root_lv
  VG Name                LFS_vg
  LV UUID                FRE4XZ-KD3K-5uIq-cbLW-wbvL-XsQi-rEucAt
  LV Write Access        read/write
  LV Creation host, time bionic, 2021-01-06 15:48:49 +0100
  LV Status              available
  # open                 1
  LV Size                20,00 GiB
  Current LE             5120
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:2
```

Les points de montages sont désormais prêts :smiley:


## Bonus : resize du volume physique

Si vous souhaitez augmenter le volume physique (dans cet exemple 50 Go): `$ sudo pvresize /dev/sdb --setphysicalvolumesize 50G`

### [Sommaire](../../README.md)
